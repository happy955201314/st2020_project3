# -*- coding: utf-8 -*
import os, time

# [Content] XML Footer Text
def test_hasHomePageNode():
    os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml')
    f = open('window_dump.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('首頁') != -1

# [Behavior] Tap the coordinate on the screen
def test_tapSidebar():
    os.system('adb shell input tap 100 100')
	

# ############################ TODOs ############################

# 1. [Content] Side Bar Text
def test_sideBarText():
    os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml')
    f = open('window_dump.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('查看商品分類') != -1
    assert xmlString.find('查訂單/退訂退款') != -1
    assert xmlString.find('追蹤/買過/看過清單') != -1
    assert xmlString.find('智慧標籤') != -1
    assert xmlString.find('其他') != -1
    assert xmlString.find('PChome 旅遊') != -1
    assert xmlString.find('線上手機回收') != -1
    assert xmlString.find('給24h購物APP評分') != -1
    f.close()
    time.sleep(3)

# 2. [Screenshot] Side Bar Text
def test_screenShotsideBarText():
	os.system('adb exec-out screencap -p > ./sideBarText.png')
	time.sleep(3)
	os.system('adb shell input keyevent 4')
	time.sleep(3)
    
# 3. [Context] Categories todo
def test_Context_Categories():
	os.system('adb shell input swipe 100 600 100 500')
	os.system('adb shell input tap 970 1600')
	time.sleep(3)
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('3C') != -1
	assert xmlString.find('周邊') != -1
	assert xmlString.find('NB') != -1
	assert xmlString.find('通訊') != -1
	assert xmlString.find('數位') != -1
	assert xmlString.find('家電') != -1
	assert xmlString.find('日用') != -1
	assert xmlString.find('食品') != -1
	assert xmlString.find('生活') != -1
	assert xmlString.find('運動戶外') != -1
	assert xmlString.find('美妝') != -1
	assert xmlString.find('衣鞋包錶') != -1
	f.close()
	time.sleep(3)
    
# 4. [Screenshot] Categories todo
def test_Screenshot_Categories():
    os.system('adb exec-out screencap -p > ./categories.png')
    time.sleep(3)

# 5. [Context] Categories page
def test_categoriesPage():
    os.system('adb shell input tap 100 1700')
    time.sleep(3)
    os.system('adb shell input tap 350 1700')
    time.sleep(3)
    os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml')
    f = open('window_dump.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('3C') != -1
    assert xmlString.find('周邊') != -1
    assert xmlString.find('NB') != -1
    assert xmlString.find('通訊') != -1
    assert xmlString.find('數位') != -1
    assert xmlString.find('家電') != -1
    assert xmlString.find('日用') != -1
    assert xmlString.find('食品') != -1
    assert xmlString.find('生活') != -1
    assert xmlString.find('運動戶外') != -1
    assert xmlString.find('美妝') != -1
    assert xmlString.find('衣鞋包錶') != -1
    f.close()
    time.sleep(3)

# 6. [Screenshot] Categories page
def test_screenShotCategoriesPage():
    os.system('adb shell input tap 350 1800')
    time.sleep(3)
    os.system('adb exec-out screencap -p > ./categoriesPage.png')
    time.sleep(3)

# 7. [Behavior] Search item “switch”
def test_searchItemSwitch():
	os.system('adb shell input tap 300 100')
	time.sleep(3)
	os.system('adb shell input text "switch"')
	time.sleep(3)
	os.system('adb shell input keyevent "KEYCODE_ENTER"')
	time.sleep(5)
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('Switch') != -1
	f.close()
	time.sleep(3)

# 8. [Behavior] Follow an item and it should be add to the list
def test_followAnItemAndItShouldBeAddToTheList():
    os.system('adb shell input tap 500 600')
    time.sleep(3)
    os.system('adb shell input tap 100 1700')
    time.sleep(3)
    os.system('adb shell input keyevent "KEYCODE_BACK"') 
    time.sleep(1)
    os.system('adb shell input keyevent "KEYCODE_BACK"')
    time.sleep(1)
    os.system('adb shell input keyevent "KEYCODE_BACK"')
    time.sleep(1)
    os.system('adb shell input tap 100 100')
    time.sleep(1)
    os.system('adb shell input tap 100 900')
    time.sleep(3)
    os.system('adb shell input swipe 500 500 500 1500')
    time.sleep(5)
    os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml')
    f = open('./window_dump.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('Switch') != -1
    f.close()
    time.sleep(3)

# 9. [Behavior] Navigate to the detail of item
def test_Behavior_NavigateToTheDetailOfItem():
    os.system('adb shell input tap 200 800')
    time.sleep(2)
    os.system('adb shell input tap 600 70')
    time.sleep(5)
    os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml')
    f = open('window_dump.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('商品特色') != -1
    f.close()
    time.sleep(3)

# 10. [Screenshot] Disconnetion Screen
def test_Screenshot_DisconnetionScreen():
	os.system('adb shell svc data disable')
	os.system('adb shell svc wifi disable')
	time.sleep(2)
	os.system('adb exec-out screencap -p > ./disconnection.png')
	time.sleep(2)
	os.system('adb shell svc data enable')
	os.system('adb shell svc wifi enable')

